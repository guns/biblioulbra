Ext.define('BiblioUlbra.model.Emprestimo', {
    extend: 'Ext.data.Model',

    config: {
        fields: [
            {name: 'id', type: 'int'},
            {name: 'agendado', type: 'string'}, 
            {name: 'biblioteca', type: 'string'},
            {name: 'descricao', type: 'string'}, 
            {name: 'numCham', type: 'string'}, 
            {name: 'taxa', type: 'string'}, 
            {name: 'titulo', type: 'string'}, 
        ]
    }
});
