Ext.define('BiblioUlbra.model.Historico', {
    extend: 'Ext.data.Model',

    config: {
        fields: [
            {name: 'id', type: 'int'},
            {name: 'agendado', type: 'string'}, 
            {name: 'biblioteca', type: 'string'},
            {name: 'devolvido', type: 'string'}, 
            {name: 'titulo', type: 'string'}, 
        ]
    }
});
