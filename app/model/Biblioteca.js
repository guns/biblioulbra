Ext.define('BiblioUlbra.model.Biblioteca', {
    extend: 'Ext.data.Model',

    config: {
        fields: [
            {name: 'id', type: 'int'},
            {name: 'biblio', type: 'string'}, 
            {name: 'paginaId', type: 'int'},
        ],

       // belongsTo: {model: 'Pagina', foreignKey: 'id'},
    }
});
