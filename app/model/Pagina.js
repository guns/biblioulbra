Ext.define('BiblioUlbra.model.Pagina', {
    extend: 'Ext.data.Model',

    config: {
        fields: [
            {name: 'id', type: 'int'},
            {name: 'ano', type: 'string'}, 
            {name: 'autor', type: 'string'},
            {name: 'edicao', type: 'string'}, 
            {name: 'titulo', type: 'string'}, 
            {name: 'bibliotecas', type: 'string'},
        ],

        //hasMany: {model: 'Biblioteca', foreignKey: 'paginaId'},
    }
});
