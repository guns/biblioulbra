Ext.define('BiblioUlbra.model.LocalStore', {
    extend: 'Ext.data.Model',

    config: {
    	idProperty: 'id',
        fields: [
            {name: 'id', type: 'int'}, 
            {name: 'cgu', type: 'string'}, 
        ]
    }
});
