Ext.define('BiblioUlbra.store.Emprestimos', {
    extend: 'Ext.data.Store',

    requires: "Ext.data.proxy.LocalStorage",

    config: {
        model: 'BiblioUlbra.model.Emprestimo',
        //autoLoad: true,
        sorters: 'id',
        // grouper: {
        //     groupFn: function(record) {
        //         return record.get('lastName')[0];
        //     }
        // },
        proxy: {
            type: 'jsonp',
            url: 'http://biblioulbra.herokuapp.com/',
            extraParams : {
                tipo: 'emprestimo',
                //cgu: '61259510',
            }
            //  reader: {
            //     type: 'json',
            // //     root: 'historico'
            //  }
            //type: 'ajax',
            //url: 'hist.json'
        },
    }
});