Ext.define('BiblioUlbra.store.LocalStore', {
    extend: 'Ext.data.Store',
    requires: "Ext.data.proxy.LocalStorage",
    config: {

        model: 'BiblioUlbra.model.LocalStore',
        
        proxy: {
            type: 'localstorage',
            id: 'biblioulbra'
        },

        sorters: [{ property: 'cgu', direction: 'ASC'}],
        grouper: {
            groupFn: function(record) {
                return record.get('cgu')[0].toUpperCase();
            }
        }
    }
});