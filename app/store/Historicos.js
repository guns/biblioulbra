Ext.define('BiblioUlbra.store.Historicos', {
    extend: 'Ext.data.Store',

    config: {
        model: 'BiblioUlbra.model.Historico',
        //autoLoad: true,
        //sorters: 'id',
        // grouper: {
        //     groupFn: function(record) {
        //         return record.get('lastName')[0];
        //     }
        // },
        proxy: {
            type: 'jsonp',
            url: 'http://biblioulbra.herokuapp.com/',
            extraParams : {
                tipo: 'historico',
                //cgu: '61259510',
            }
            //  reader: {
            //     type: 'json',
            // //     root: 'historico'
            //  }
            //type: 'ajax',
            //url: 'hist.json'
        },
    }
});