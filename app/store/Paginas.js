Ext.define('BiblioUlbra.store.Paginas', {
    extend: 'Ext.data.Store',

    config: {
        model: 'BiblioUlbra.model.Pagina',
                
        //autoLoad: true,
        //sorters: 'id',
        //defaultRootProperty: 'bibliotecas',
        
        proxy: {
            type: 'jsonp',
            url: 'http://biblioulbra.herokuapp.com/',
            //root: 'paginas',
            /*reader: {
                type: 'json', //json ou xml
                root: 'paginas'
            },*/
            extraParams : {
                tipo: 'buscar',
                 // adjacent: 'N',
                 // find_code: 'WRD',
                 // local_base: 'ULB01',
                 // palavra: 'teste',
                 // numpag: '2',
            }
        },
    }
});