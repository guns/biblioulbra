Ext.define('BiblioUlbra.controller.Aplicacao', {
    extend: 'Ext.app.Controller',

    config: {

        stores: ['LocalStore'],

        models: ['LocalStore'],

        refs: {

            main: '#main',
            configuracao: "configuracao",
            EmprestTab: '#EmprestTab',
            HistTab: '#HistTab',
            busca: 'busca',
            buscaResult: 'buscaresult',
            ResultTab: '#ResultTab',
            BuscaTab: '#BuscaTab',
        },

        control: {
            'main button[action=renovarButton]': {
                tap: 'onEmprstRenov'
            },

            'main button[action=limparButton]': {
                tap: 'onLimpaBusca'
            },

            'main button[action=salvarButton]': {
                tap: 'onConfigSalvar'
            },

            'main button[action=BuscaButton]': {
                tap: 'onBuscaLivros'
            },

            'main button[action=ProximoButton]': {
                tap: 'onProximaPagina'
            },

            'main button[action=AnteriorButton]': {
                tap: 'onAnteriorPagina'
            },

            EmprestTab: {
                activate: 'onEmprestTab'
            },

            HistTab: {
                activate: 'onHistTab'
            },

            BuscaTab: {
                activate: 'onBuscaTab'
            }
        }
    },

    slideLeftTransition: { type: 'slide', direction: 'left' },
    slideRightTransition: { type: 'slide', direction: 'right' },

    onAnteriorPagina:function(){
        var numPag = Ext.ComponentQuery.query('main #numPag')[0].getValue();
        if(numPag <= 1)
        {
            Ext.ComponentQuery.query('main #resultBusca')[0].hide();
            Ext.ComponentQuery.query('main #dadosBusca')[0].show();
            Ext.ComponentQuery.query('main #tabResult')[0].hide();
            Ext.ComponentQuery.query('main #tabBusca')[0].show();

            Ext.ComponentQuery.query('main #numPag')[0].setValue(1);
        }
        else
        {
            this.onBuscaPagina('ant');
        }
    },

    onProximaPagina:function(){
        this.onBuscaPagina('prox');
    },

    onBuscaPagina:function(tipo){
        var palav = Ext.ComponentQuery.query('buscaform #palavras')[0].getValue();
        var findCode = Ext.ComponentQuery.query('buscaform #comboCampos')[0].getValue();
        var adjac = Ext.ComponentQuery.query('buscaform #exata')[0].getValue();
        var localBase = Ext.ComponentQuery.query('buscaform #comboCatalago')[0].getValue();
        
        var numPag = Ext.ComponentQuery.query('main #numPag')[0].getValue();
        
        if(tipo == 'prox')
        {
            numPag += 1;
        }
        else if(tipo == 'ant')
        {
            numPag -= 1;
        }
        
        Ext.ComponentQuery.query('main #numPag')[0].setValue(numPag);

        if(adjac == 0)
        {
            adjacen = 'Y';
        }
        else if(adjac == 1)
        {
            adjacen = 'N';
        }

        var paginasStore = Ext.getStore('Paginas');
        paginasStore.load({
            params:{
                adjacent: adjacen,
                find_code: findCode,
                local_base: localBase,
                palavra: palav,
                numpag: numPag,
            }
        });
        Ext.ComponentQuery.query('main #numRegistr')[0].setHtml('Página '+numPag);
    },

    onBuscaTab:function(view){
        this.onLimpaBusca();

        Ext.ComponentQuery.query('main #resultBusca')[0].hide();
        Ext.ComponentQuery.query('main #dadosBusca')[0].show();
        Ext.ComponentQuery.query('main #tabResult')[0].hide();
        Ext.ComponentQuery.query('main #tabBusca')[0].show();

        Ext.ComponentQuery.query('main #numPag')[0].setValue(1);
    },  

    onLimpaBusca:function(){
        Ext.ComponentQuery.query('buscaform #palavras')[0].setValue("");
        Ext.ComponentQuery.query('buscaform #comboCampos')[0].reset();
        Ext.ComponentQuery.query('buscaform #exata')[0].reset();
        Ext.ComponentQuery.query('buscaform #comboCatalago')[0].reset();
        
        //console.log(Ext.getStore('Paginas').getAllCount());
    },

    onBuscaLivros:function(){

        var palav = Ext.ComponentQuery.query('buscaform #palavras')[0].getValue();
        var findCode = Ext.ComponentQuery.query('buscaform #comboCampos')[0].getValue();
        var adjac = Ext.ComponentQuery.query('buscaform #exata')[0].getValue();
        var localBase = Ext.ComponentQuery.query('buscaform #comboCatalago')[0].getValue();

        //console.log(palav);
        if(palav == "")
        {
            var msg = 'Informe alguma palavra para pesquisar.';
               
            Ext.Msg.alert('Informação!', msg);
        }
        else
        {
            var numPag = Ext.ComponentQuery.query('main #numPag')[0].getValue();

            if(adjac == 0)
            {
                adjacen = 'Y';
            }
            else if(adjac == 1)
            {
                adjacen = 'N';
            }

            var paginasStore = Ext.getStore('Paginas');
            paginasStore.load({
                params:{
                    adjacent: adjacen,
                    find_code: findCode,
                    local_base: localBase,
                    palavra: palav,
                    numpag: numPag,
                }
            });
            console.log(paginasStore.data);
            Ext.ComponentQuery.query('main #dadosBusca')[0].hide();
            Ext.ComponentQuery.query('main #resultBusca')[0].show();
            Ext.ComponentQuery.query('main #tabResult')[0].show();
            Ext.ComponentQuery.query('main #tabBusca')[0].hide();
            Ext.ComponentQuery.query('main #numRegistr')[0].setHtml('Página '+numPag);
        }
    },

    onEmprstRenov: function(view, item) {
        var EmprestimosStore = Ext.getStore('Emprestimos');
        if(EmprestimosStore.getAllCount() <= 0)
        {
            Ext.Msg.alert('Informação', 'Não existe livros para renovar');
        } else {
            var loading = Ext.create('Ext.LoadMask',{
                xtype: 'loadmask',
                message: 'Renovando os Empréstimos..',
                indicator: true
            });
            loading.show();
            var CGU = Ext.ComponentQuery.query('configuracao #CGU')[0].getValue();
            Ext.data.JsonP.request({
                url: 'http://biblioulbra.herokuapp.com/',
                //callbackKey: 'callback',
                params: {
                    tipo: 'renovar',
                    cgu: CGU,
                },
                success: function(result, request) {
                    //console.log(request);
                    loading.hide();
                    if(result.titulo == " Empréstimos renovados: ")
                    {
                        Ext.Msg.alert('Renovação', 'Empréstimos Renovados com Sucesso!');
                        var EmprestimosStore = Ext.getStore('Emprestimos');
                        EmprestimosStore.load({
                            params: { cgu: CGU }
                        });
                    }
                    else if (result.titulo == " Empréstimos NÃO renovados: ")
                    {   var msg = '* Motivos da não renovação:<br>';
                            msg += '<br>- item já está renovado ou não aceita renovação;';
                            msg += '<br>- item possui reserva(s);';
                            msg += '<br>- período de empréstimo do item (6 meses) excedido;';
                            msg += '<br>- um ou mais itens em atraso;';
                            msg += '<br>- há débitos em seu nome;';
                            msg += '<br>- há problemas em seu cadastro.';
                            msg += '<br>* Para mais esclarecimentos, entre em contato com a Biblioteca de sua unidade.';
                        Ext.Msg.alert('Empréstimos Não renovados!', msg);
                    }
                    else {
                        Ext.Msg.alert('Renovação', 'Tem algum Probelma');
                    }
                }
            });
        }
    },

    onEmprestTab: function() {
        
        //console.log('Clicou nos emprestimos');
        var CGU = Ext.ComponentQuery.query('configuracao #CGU')[0].getValue();
        var EmprestimosStore = Ext.getStore('Emprestimos');
        if(EmprestimosStore.getAllCount() <= 0)
        {
            EmprestimosStore.removeAll();
            EmprestimosStore.load({
                params: { cgu: CGU }
            });
        } 
    },

    onHistTab: function() {
        
        //console.log('Clicou nos emprestimos');
        var CGU = Ext.ComponentQuery.query('configuracao #CGU')[0].getValue();
        var HistoricosStore = Ext.getStore('Historicos');
        if(HistoricosStore.getAllCount() <= 0)
        {
            HistoricosStore.removeAll();
            HistoricosStore.load({
                params: { cgu: CGU }
            });
        } 
    },

    // Metodo que salva as configurações 
    onConfigSalvar: function(view, item) {

        var CGU = Ext.ComponentQuery.query('configuracao #CGU')[0].getValue();

        var LocalStore = Ext.getStore("LocalStore");

        LocalStore.removeAll();

        LocalStore.add({cgu: CGU});

        LocalStore.sync();
        //LocalStore.sort([{ property: 'cgu', direction: 'ASC'}]);
        //console.log(CGU);
        Ext.Msg.alert('Configuração', 'Configurações Salvas com Sucesso!');

        var EmprestimosStore =  Ext.getStore("Emprestimos");
        EmprestimosStore.load({
            params: { cgu: CGU }
        });

        var HistoricosStore =  Ext.getStore("Historicos");
        HistoricosStore.load({
            params: { cgu: CGU }
        });
        
    }
});
