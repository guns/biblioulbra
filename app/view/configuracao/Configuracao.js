Ext.define('BiblioUlbra.view.configuracao.Configuracao', {
    extend: 'Ext.form.FieldSet',
    //extend: 'Ext.DataView',
    xtype: 'configuracao',

    config: {
        //title: 'Historico de livros',
        //cls: 'x-contacts',

        instructions: ["<p align='center'> O <b>BiblioUlbra</b> aplicativo móvel de busca e renovação de livros nas bibliotecas da ULBRA.</p><br>",
                    "<p align='center'>Gustavo Bauer Machado:<br> ",
                    "E-mail: <i>gustbauer@gmail.com</i><br>",
                    "Twitter: <i>gunsMachado</i></p>"].join(""),
            items: [
                {
                    xtype: 'textfield',
                    name : 'cgu',
                    label: 'CGU:',
                    itemId: 'CGU',
                    required: true,
                    //value: '61259510'
                },{
                    xtype: 'textfield',
                    name : 'id',
                    label: 'id:',
                    hidden:true,
                    itemId: 'idconfig'
                }
            ]
    }
});
