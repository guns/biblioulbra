Ext.define('BiblioUlbra.view.emprestimo.EmprestimoDataView', {
    extend: 'Ext.Panel',
    requires: 'Ext.DataView',
    //extend: 'Ext.DataView',
    xtype: 'emprestimodataview',

    config: {

        fullscreen: true,
        store: {
            fields: ['titulo'],
            data: [
                {titulo: '<b>* Motivos da não renovação:</b>'},
                {titulo: '&nbsp- item já está renovado ou não aceita renovação;'},
                {titulo: '&nbsp- item possui reserva(s);'},
                {titulo: '&nbsp- período de empréstimo do item (6 meses) excedido;'},
                {titulo: '&nbsp- um ou mais itens em atraso;'},
                {titulo: '&nbsp- há débitos em seu nome;'},
                {titulo: '&nbsp- há problemas em seu cadastro.'},
                {titulo: '<br>'},
                {titulo: '<b>* Prezado ALUNO, a data limite para empréstimos e renovações de materias da Biblioteca é a data final do período letivo da sua última matrícula confirmada.'},
                {titulo: '<b>* Para mais esclarecimentos, entre em contato com a Biblioteca de sua unidade.'},
            ]
        },

    itemTpl: '<p>&nbsp{titulo}</p>'
    }
});
