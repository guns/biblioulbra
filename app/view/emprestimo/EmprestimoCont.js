Ext.define("BiblioUlbra.view.emprestimo.EmprestimoCont", {
    extend: "Ext.Container",
    alias: "widget.emprestimocontainer",

    requires: ["BiblioUlbra.view.emprestimo.EmprestimoDataView",
                "BiblioUlbra.view.emprestimo.Emprestimo"],

    initialize: function () {

        this.callParent(arguments);

        var dataView = {
            xtype: "emprestimodataview",
        };

        var notesList = {
            xtype: "emprestimo",
            //store: Ext.getStore("")
        };

        this.add([dataView, notesList]);
    },

    config: {
        layout: {
            type: 'fit'
        }
    }
});