Ext.define('BiblioUlbra.view.emprestimo.Emprestimo', {
    extend: 'Ext.dataview.List',
    //extend: 'Ext.DataView',
    xtype: 'emprestimo',

    config: {
        //title: 'Historico de livros',
        //cls: 'x-contacts',

        fullscreen: true,
        cls: 'twitterView',
        itemId: 'EmprestData',

        store: 'Emprestimos',
        // itemTpl: [
        //     '{biblioteca} {agendado}',
        //     '<span>{titulo}</span>'
        // ].join('')
        loadingText: "Carregando Empréstimos...",
        emptyText: "<div class=\"contatos-list-empty-text\">Nenhum empréstimo encontrado.</div>",
        //onItemDisclosure: true,
        //grouped: true,

        itemTpl: ['<h1><b>{titulo}</b></h1>',
        '<p><i>Agendado para:</i> {agendado}</p>',
        '<p><i>Biblioteca:</i> {biblioteca}</p>',
        '<p><i>No. Chamada:</i> {numCham}</p>',
        '<p><i>Taxa por atraso:</i> {taxa}</p>',
        '<p>{descricao}</p>',
        '<div style="clear: both"></div>'].join('')
    }
});
