Ext.define('BiblioUlbra.view.historico.Historico', {
    extend: 'Ext.List',
    //extend: 'Ext.DataView',
    xtype: 'historico',

    config: {
        //title: 'Historico de livros',
        //cls: 'x-contacts',

        fullscreen: true,
        cls: 'twitterView',

        store: 'Historicos',
        // itemTpl: [
        //     '{biblioteca} {agendado}',
        //     '<span>{titulo}</span>'
        // ].join('')
        loadingText: "Carregando Histórico...",
        emptyText: "<div class=\"contatos-list-empty-text\">Nenhum histórico encontrado.</div>",
        //onItemDisclosure: true,
        //grouped: true,
        itemTpl: '<h1><b>{titulo}</b></h1><p>Agendado para: {agendado}</p><p>Devolvido em: {devolvido}</p></p><p>Biblioteca: {biblioteca}</p><div style="clear: both"></div>'
    }
});
