Ext.define("BiblioUlbra.view.Main", {
    extend: 'Ext.navigation.View',
    extend: 'Ext.tab.Panel',
    requires: [
        'Ext.TitleBar',
        'Ext.form.FieldSet',
        'Ext.field.Select',
        'Ext.field.Hidden',
        'Ext.field.Checkbox',
        'Ext.field.Number',
        'Ext.Label',
        'Ext.Img',
        'Ext.List',
        'Ext.field.Toggle',
        'Ext.LoadMask',
        'BiblioUlbra.view.historico.Historico',
        'BiblioUlbra.view.emprestimo.Emprestimo',
        'BiblioUlbra.view.configuracao.Configuracao',
        'BiblioUlbra.view.busca.Busca'
        //'Ext.Video'
    ],
    alias: "widget.main",
    config: {
        tabBarPosition: 'bottom',

        items: [
            {
                title: 'Home',
                iconCls: 'home',

                styleHtmlContent: true,
                scrollable: true,
                layout: 'vbox',

                items: [{
                    docked: 'top',
                    xtype: 'titlebar',
                    title: 'BiblioUlbra'
                },{
                    xtype: 'container',
                    layout: 'hbox',
                    flex: 1,
                    items: [{
                        xtype: 'image',
                        src: 'resources/images/Icon~ipad.png',
                        flex: 1,
                    },{
                        xtype: 'panel',
                        flex: 3,
                        html: ["<h3 align='center'>Bem vindo ao <br><b>BiblioUlbra</b>.</h3>",]
                    }]
                },{
                    xtype: 'panel',
                    flex: 3,
                    html: [
                    "<br><p align='center'>Aplicativo móvel de busca e renovação de livros nas bibliotecas da ULBRA.</p>",
                    "<p align='center'>Desenvolvido pelo acadêmico Gustavo Bauer Machado, ",
                    "como Trabalho de Conclusão do Curso de Sistemas de Informação da ULBRA - Torres. ",
                    "Orientado pelo Professor Ramon dos Santos Lummertz</p>"].join(""),
                }/*,{
                    xtype: 'image',
                    src: 'resources/images/si.png',
                    flex: 1,
                }*/],

                /*html: [
                    '<div class="headshot" style="background-image:url(resources/images/si.png);"></div>',
                    " <br><br>",
                    "<p align='center'> Bem vindo ao BiblioUlbra.</p>",
                    "<p align='center'>Aplicativo móvel de busca e renovação de livros nas bibliotecas da ULBRA.</p><br>",
                    "<p align='center'>Desenvolvido pelo academico Gustavo Bauer Machado, ",
                    "como Trabalho de Conclusão do Curso de Sistemas de Informação da UlBRA - Torres. ",
                    "Orientado pelo Professor Ramon dos Santos Lummertz</p><br>"

                ].join("")*/
            },{
                title: 'Buscar',
                iconCls: 'search',
                layout: 'card',
                id: 'BuscaTab',
                itemId: 'TabBusca',
                scrollable: true,

                items: [{
                        docked: 'top',
                        xtype: 'titlebar',
                        title: 'Busca de Livro',
                    },{
                        xtype: 'container',
                        layout: 'fit',
                        items: [{
                            xtype:'buscaform',
                            itemId: 'dadosBusca',
                            //disabled: false,
                        },{
                            xtype: 'buscaresult',
                            itemId: 'resultBusca',
                            //disabled: true,
                        }]
                        
                    },{
                        xtype:'toolbar',
                        docked: 'bottom',
                        padding: 10,
                        itemId: 'tabBusca',
                        layout: {
                            type: 'hbox',
                            align: 'center'
                        },
                        items: [{ 
                            xtype: 'spacer' 
                        },{
                            text: 'Limpar',
                            ui: 'decline',
                            action: 'limparButton',
                        },{
                            text: 'Buscar',
                            ui: 'confirm',
                            action: 'BuscaButton'
                        }
                        ]
                    },{
                        xtype:'toolbar',
                        docked: 'bottom',
                        itemId: 'tabResult',
                        padding: 10,
                        layout: {
                            type: 'hbox',
                            align: 'center'
                        },
                        items: [{
                            text: 'Anterior',
                            ui: 'action',
                            action: 'AnteriorButton',
                        },{ 
                            xtype: 'spacer' 
                        },{
                            xtype: 'label',
                            itemId: 'numRegistr',
                            html: 'My label!'
                        },{
                            xtype: 'numberfield',
                            itemId: 'numPag',
                            hidden: true,
                            value: 1,
                            //html: 'My label!'
                        },{ 
                            xtype: 'spacer' 
                        },{
                            text: 'Proximo',
                            ui: 'action',
                            action: 'ProximoButton'
                        }
                        ]
                    }
                ]
            },{
                title: 'Empréstimos',
                iconCls: 'bookmarks',
                layout: 'card',
                scrollable: true,
                id: 'EmprestTab',

                //styleHtmlContent: true,
                //scrollable: true,

                items: [{
                    docked: 'top',
                    xtype: 'titlebar',
                    title: 'Empréstimos Atuais'
                },{
                    xtype: 'emprestimo',
                },{
                    xtype:'toolbar',
                    docked: 'bottom',
                    padding: 10,
                    layout: {
                        type: 'hbox',
                        align: 'center'
                    },
                    items: [{ 
                        xtype: 'spacer' 
                    },{
                        text: 'Renovar Empréstimos',
                        ui: 'confirm',
                        action: 'renovarButton',
                        //itemId: 'renovarLivros',
                        //disabled: true,
                    }
                    ]
                }],
            },{
                title: 'Histórico',
                iconCls: 'time',
                layout: 'card',
                scrollable: true,
                id: 'HistTab',

                //styleHtmlContent: true,
                //scrollable: true,

                items: [{
                    docked: 'top',
                    xtype: 'titlebar',
                    title: 'Histórico de Empréstimos'
                },{
                    xtype: 'historico',
                }],
            },{
                title: 'Configurações',
                iconCls: 'settings',
                layout: 'vbox',

                items: [{
                    docked: 'top',
                    xtype: 'titlebar',
                    title: 'Configurações'
                },{
                    xtype: 'configuracao',
                    flex: 2,
                    //title: 'Configurações',
                },{
                    xtype: 'image',
                    src: 'resources/images/si.png',
                    flex: 1,
                    // right : '30%',
                    // left: '30%'
                },{
                        xtype:'toolbar',
                        docked: 'bottom',
                        padding: 10,
                        layout: {
                            type: 'hbox',
                            align: 'center'
                        },
                        items: [{ 
                            xtype: 'spacer' 
                        },{
                            text: 'Salvar',
                            ui: 'confirm',
                            action: 'salvarButton'
                        }
                        ]
                    }],
            }
        ]
    }
});
