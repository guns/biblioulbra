Ext.define("BiblioUlbra.view.busca.BuscaForm", {
    extend: "Ext.form.FieldSet",
    //alias: "widget.busca",
     xtype: 'buscaform',
     layout: 'fit',

    config: {
        //title: 'Historico de livros',
        //cls: 'x-contacts',

        instructions: 'Informe os dados para realizar a busca',
        items: [
            {
                xtype: 'textfield',
                name: 'palavras',
                placeHolder: 'Informe Palavras',
                itemId: 'palavras',
                //label: 'Nome',
                required: true
            },{
                xtype: 'selectfield',
                name: 'campos',
                itemId: 'comboCampos',
                options: [
                    {text: 'Todos os Campos', value: 'WRD'},
                    {text: 'Autor', value: 'WAU'},
                    {text: 'Título', value: 'WTI'},
                    {text: 'Assunto', value: 'WSU'},
                    {text: 'Série', value: 'WSE'},
                    {text: 'Nota Tese/Diss/Monog', value: 'WTE'},
                    {text: 'Código de Barras', value: 'BAR'}
                ]
            },{
                xtype: 'togglefield',
                name: 'exata',
                label: 'Buscar frase exata?',
                itemId: 'exata',
                labelWidth: '65%'
            },{
                xtype: 'selectfield',
                name: 'Catálago',
                itemId: 'comboCatalago',
                options: [
                    {value: 'ULB01', text: 'Todas Bibliotecas da ULBRA'},
                    {value: 'CANOAS', text: 'Campus Canoas'},
                    {value: 'CACHOEIRA', text: 'Campus Cachoeira do Sul'},
                    {value: 'CARAZINHO', text: 'Campus Carazinho'},
                    {value: 'GRAVATAI', text: 'Campus Gravataí'},
                    {value: 'GUAIBA', text: 'Campus Guaíba'},
                    {value: 'PORTOALEGRE', text: 'Campus Porto Alegre'},
                    {value: 'SANTAMARIA', text:'Campus Santa Maria'},
                    {value: 'SAOJER', text: 'Campus São Jerônimo'},
                    {value: 'TORRES', text: 'Campus Torres'},
                    {value: 'HUC', text: 'Hospital Universitário de Canoas'},
                    {value: 'SCONCORDIA', text: 'Seminário Concórdia'},
                    {value: 'JIPARANA', text: 'Centro Universitário de Ji-Paraná'},
                    {value: 'MANAUS', text: 'Centro Universitário de Manaus'},
                    {value: 'PALMAS', text: 'Centro Universitário de Palmas'},
                    {value: 'SANTAREM', text: ' Centro Universitário de Santarém'},
                    {value: 'ITUMBIARA', text: 'ILES de Itumbiara'},
                    {value: 'PORTOVELHO', text: 'ILES de Porto Velho'},
                    {value: 'CONCORDIA', text: 'UE Concórdia'},
                    {value: 'CRISTO', text: 'UE Cristo Redentor'},
                    {value: 'ESP-CONCORDIA', text: ' UE Especial Concórdia'},
                    {value: 'UEML', text: 'UE Martinho Lutero'},
                    {value: 'PAZ', text: 'UE Paz'},
                    {value: 'SAOJOAO', text: 'UE São João'},
                    {value: 'SAOLUCAS ', text: 'UE São Lucas'},
                    {value: 'SAOMARCOS', text: 'UE São Marcos'},
                    {value: 'SAOMATEUS', text: 'UE São Mateus'},
                    {value: 'SAOPEDRO', text: 'UE São Pedro'},
                    {value: 'CEML', text: 'UE CEML'},
                    {value: 'APLICACAO', text: 'UE Escola Aplicação'},
                    {value: 'ANTARES ', text: 'UE Colégio Antares'},
                    {value: 'CEDUCS', text: 'UE CEDUCS'},
                    {value: 'CEDUSP', text: 'UE CEDUSP'},
                    {value: 'LIVRO', text: 'Livros, Folhetos, Obras de Referência'},
                    {value: 'PERIODICO', text: 'Periódicos, Revistas, Jornais'},
                    {value: 'MULTIMEIO', text: 'Multimeios (Materiais Especiais)'},
                    {value: 'TCC', text: 'TCCs - Trabalhos de Conclusão de Cursos'},
                    {value: 'MONO', text: 'Monografias de Especialização'},
                    {value: 'TESE', text: 'Teses e Dissertações'},
                    {value: 'BDMONO', text: 'BDMONO - Monografias de Especialização On-line'},
                    {value: 'BDTESE', text: 'BDTD - Teses e Dissertações On-line'},
                    {value: 'ARTIGO', text: 'Artigos de Periódicos'},
                    {value: 'PRODCIENT', text: 'Produção Científica'},
                    {value: 'NORMA', text: 'Normas Técnicas'},
                    {value: 'BASEDADOS', text: 'Bases de Dados'},
                    {value: 'ONLINE', text: 'Documentos On-line'},
                ]
            }
        ]
    }
});

