Ext.define("BiblioUlbra.view.busca.BuscaList", {
    extend: "Ext.dataview.NestedList",
    alias: "widget.buscalist",

    //displayField: 'titulo',
    config: {
        title: 'Groceries',
        loadingText: "Carregando Contatos...",
        emptyText: "<div class=\"contatos-list-empty-text\">Nenhum contato encontrado.</div>",
        onItemDisclosure: true,
        grouped: true,
        store: 'Paginas',
        itemTpl: "<div class=\"list-item-title\">{titulo}</div>"       
    }
});