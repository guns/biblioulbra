Ext.define("BiblioUlbra.view.busca.BuscaResult", {
    extend: "Ext.List",
    //alias: "widget.buscaresult",
    xtype: 'buscaresult',
    
    config: {
        fullscreen: true,
        //layout: 'fit',
        //scrollable: true,
        cls: 'twitterView',
        store: 'Paginas',

        loadingText: "Realizando a Busca...",
        emptyText: "<div class=\"contatos-list-empty-text\">Nenhum registro encontrado.</div>",
        //onItemDisclosure: true,
        
        itemTpl: ['<h1><b>{titulo}</b></h1>',
            '<p><i>Autor: </i> {autor}</p>',
            '<p><i>Ano:</i> {ano}  <i>Edição:</i> {edicao}</p>',
            '<p><i>Bibliotecas Disponíveis:</i> <u>{bibliotecas}</u></p>'].join('').replace("u'None'", "")
    }
});