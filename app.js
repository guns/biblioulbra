// Ext.Loader.setPath({
//     'Ext': '../../sencha-touch-2/src'
// });

Ext.application({
    name: 'BiblioUlbra',

    requires: [
        'Ext.MessageBox',
        'Ext.Logger',
        'Ext.LoadMask'
    ],

    models: ['Historico', 'Emprestimo', 'LocalStore', 'Pagina'],

    stores: ['Historicos', 'Emprestimos', 'LocalStore', 'Paginas'],

    views: ['Main', 
            'historico.Historico',
            'emprestimo.Emprestimo',
            'busca.Busca',
            'configuracao.Configuracao',
            'busca.BuscaResult',
            'busca.BuscaList',
            'busca.BuscaForm'],

    controllers: ['Aplicacao'],

    //isIconPrecomposed: true,
    phoneStartupScreen: 'resources/loading/320x460.png',
    tabletStartupScreen: 'resources/loading/768x1004.png',

    glossOnIcon: false,
    icon: {
        '57': 'resources/icons/Icon.png',
        '72': 'resources/icons/Icon~ipad.png',
        '114': 'resources/icons/Icon@2x.png',
        '144': 'resources/icons/Icon~ipad@2x.png'
    },

    isIconPrecomposed: true,

    /*startupImage: {
        '320x460': 'resources/startup/320x460.jpg',
        '640x920': 'resources/startup/640x920.png',
        '768x1004': 'resources/startup/768x1004.png',
        '748x1024': 'resources/startup/748x1024.png',
        '1536x2008': 'resources/startup/1536x2008.png',
        '1496x2048': 'resources/startup/1496x2048.png'
    },*/

    launch: function() {
        Ext.Viewport.add({
            xclass: 'BiblioUlbra.view.Main'
        });

        var CGU;
        var LocalStore = Ext.getStore("LocalStore");
        LocalStore.load(
            function(records, op, success){
                
                var lista, i;

                for (i=0; i<records.length; i++){
                    lista = records[i].data;
                    CGU = lista.cgu;
                    //console.log(CGU);
                }
            });
        // if(CGU == null)
        // {
        //     Ext.Msg.prompt('CGU', 'Informe o seu CGU:', function(text) {
        //         CGU = text;
        //     });
        // }
        // console.log(CGU);
        Ext.ComponentQuery.query('configuracao #CGU')[0].setValue(CGU);
    },

    onUpdated: function() {
        Ext.Msg.confirm(
            "Application Update",
            "This application has just successfully been updated to the latest version. Reload now?",
            function(buttonId) {
                if (buttonId === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});
